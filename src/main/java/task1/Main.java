package task1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final String PATH="task1.txt";
    public static void main(String[] args) throws ClassNotFoundException{
        serialize();
        deserialize();

    }


        private static void serialize(){
            List<Droid> droids=new ArrayList<>();
            droids.add(new Droid(100,64));
            droids.add(new Droid(79,99));
            droids.add(new Droid(86,95));
            Ship ship=new Ship(droids,"SuperStar");
            try (ObjectOutputStream objectOutputStream=new ObjectOutputStream((new FileOutputStream(PATH)))){
                objectOutputStream.writeObject(ship);
            }catch (IOException e){
                System.out.println("Output error");
            }
        }

    private static void deserialize() throws ClassNotFoundException{
        try (ObjectInputStream objectInputStream=new ObjectInputStream(new FileInputStream(PATH))){
            Ship ship=(Ship) objectInputStream.readObject();
            System.out.printf("Droids: %s%nName: %s", ship.getDroids(), ship.getName());
        }catch (IOException e1){
            System.out.println("Input error");
        }
    }

}
