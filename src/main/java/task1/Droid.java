package task1;

import java.io.Serializable;

public class Droid implements Serializable {
    private int hp;
    private int power;

    public Droid(int hp, int power) {
        this.hp = hp;
        this.power = power;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
