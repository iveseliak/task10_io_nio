package test;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        String path="C:\\Users\\ivan\\IdeaProjects\\task07\\task07\\task10_IO_NIO\\data.txt";
        File file=new File(path);

        System.out.println(file.getName());
        System.out.println(file.getAbsolutePath());

        System.out.println(file.getParent());
        System.out.println(file.isFile());
        System.out.println(file.canRead());
        System.out.println(file.canWrite());
        System.out.println(file.isHidden());
        System.out.println(CapacityFormatter.toGigabytes(file.getTotalSpace())+"Gb");
        System.out.println(CapacityFormatter.toGigabytes(file.getFreeSpace())+"gb");
    }

    private static class CapacityFormatter{
        private static long toGigabytes(long capacity){
            return capacity/(long) Math.pow(10,9);
        }
    }
}
