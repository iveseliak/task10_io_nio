package test.OSExample;

import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        String text="Hello world!";
        byte[] bytes=text.getBytes();
        try (FileOutputStream stream=new FileOutputStream("file.txt")){
            for (byte eachByte:bytes){
                stream.write(eachByte);
            }
        }catch (IOException e){
            System.out.println("Output error");
        }
    }
}
