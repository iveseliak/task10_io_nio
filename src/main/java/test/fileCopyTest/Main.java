package test.fileCopyTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try (FileInputStream stream=new FileInputStream("file.txt")){
            File directory=new File("src/folder");
            if ((!directory.exists())) directory.mkdir();

            try (FileOutputStream stream1=new FileOutputStream("src/folder/file.txt")){
                byte[] bytes=new byte[stream.available()];
                int length;

                while((length=stream.read(bytes)) != -1){
                    stream1.write(bytes, 0, length);
                }
            }catch (IOException e1){
                System.out.println("Output error");
            }
        }catch (IOException e){
            System.out.println("Input Exception");
        }
    }
}
