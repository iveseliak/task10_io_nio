package test.symbolWriterTest;

import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        String text="Hello world!";
        char[] chars=new char[text.length()];
        text.getChars(0,chars.length,chars,0);

        try (FileWriter writer=new FileWriter("file1.txt")){
            for(char c:chars){
                writer.write(c);
            }
        }catch (IOException e){
            System.out.println("Output error");
        }
    }
}
