package test.symbolReaderTest;

import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try (FileReader fileReader=new FileReader("file.txt")){
            int symbol;
            while ((symbol=fileReader.read())!=-1){
                System.out.print((char) symbol);
            }
        }catch (IOException e){
            System.out.println("Input error");
        }
    }
}
