package task2;

import java.io.*;
import java.util.Date;

public class FileReadingManager {
    public void defaultFileReader(){
        StringBuilder string = new StringBuilder();
        File file = new File("English_File_3e_-_Int_SB(1).pdf");
        try(FileReader br =  new FileReader(file)) {
            String s;
            int data = br.read();
            long before = new Date().getTime();
            while( data !=-1){
                string.append(((char)data));
                data = br.read();
            }
            long after = new Date().getTime();
            System.out.println((after-before)+"мс");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void dafaultBufferReader(){
        StringBuilder string = new StringBuilder();
        File file = new File("English_File_3e_-_Int_SB(1).pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file))) {
            String s;
            long before = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long after = new Date().getTime();
            System.out.println((after-before) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReader_2MB(){
        StringBuilder string = new StringBuilder();
        File file = new File("English_File_3e_-_Int_SB(1).pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file), 2048)) {
            String s;
            long before = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long after = new Date().getTime();
            System.out.println((after-before) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReader_4MB(){
        StringBuilder string = new StringBuilder();
        File file = new File("English_File_3e_-_Int_SB(1).pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file), 1024*4)) {
            String s;
            long before = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long after = new Date().getTime();
            System.out.println((after-before) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReader_8MB(){
        StringBuilder string = new StringBuilder();
        File file = new File("English_File_3e_-_Int_SB(1).pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file), 1024*8)) {
            String s;
            long before = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long after = new Date().getTime();
            System.out.println((after-before) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReaderWithInputStreamReader(){
        StringBuilder string = new StringBuilder();
        File file = new File("English_File_3e_-_Int_SB(1).pdf");
        try (BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String s;
            long before = new Date().getTime();
            while ((s = bf.readLine()) != null) {
                string.append(s);
            }
            long after = new Date().getTime();
            System.out.println((after-before) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
