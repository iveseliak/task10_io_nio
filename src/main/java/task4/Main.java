package task4;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        String path="src/backup";
        File directory=new File(path);

        if (directory.isDirectory()){
            String[] content=directory.list();
            if (content!=null){
                for (String eachFile:content){
                    File file=new File(path+"/"+eachFile);
                    System.out.println(file.isDirectory()?(eachFile+" is directory"):(eachFile+" is file"));
                }
            }
        }

//        if (!directory.exists()){
//            System.out.println(directory.mkdir()?"Successfully":"Failed");
//        }else{
//            System.out.println("Already exist");
//        }


    }
}
