package task3;

import java.io.*;

public class FileManager {
    File file=new File("file1.txt");
    public void readFile() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
        while(reader.ready()) {
            String line = reader.readLine();
            if (line.contains("//")){
                System.out.println(line);
            }
        }
    }
}
